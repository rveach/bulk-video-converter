FROM python:3.7

WORKDIR /src

RUN apt-get update \
    && apt-get install -y ffmpeg \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir nose pyinstaller nose-htmloutput

COPY src/bulk_converter.py /src/bulk_converter.py
