#! /usr/bin/env python

import argparse
import glob
import json
import os
import subprocess
import tempfile

from pathlib import Path

class UnableToDeleteFile(Exception):
    pass

class MissingFFProbeInfo(Exception):
    pass

class FoundVideo:

    @staticmethod
    def is_file_ok(filepath):
        """
            quick check to make sure a file is present and has a size 
        """

        if not filepath:
            # return False if passed a None value
            return False

        elif not os.path.exists(filepath):
            # Return False if the file does not exist
            return False

        elif os.stat(filepath).st_size == 0:
            # Return False if the file is 0 bytes
            return False

        else:
            # all other checks passed, return True
            return True

    def delete_replacement(self):
        if self.replacement and self.is_file_ok(self.video):
            os.remove(self.replacement)
            self.replacement = None
        else:
            raise UnableToDeleteFile("Did not delete replacemnt - problem with original found.")
    
    def delete_original(self):
        if self.video and self.is_file_ok(self.replacement) and self.archive_success:
            os.remove(self.video)
            self.video = None
        else:
            raise UnableToDeleteFile("Did not delete original - problem with replacement found.")

    def delete_largest(self):
        if self.video and self.replacement:

            orig_size = os.stat(self.video).st_size
            repl_size = os.stat(self.replacement).st_size

            if orig_size > repl_size:
                self.delete_original()
            else:
                self.delete_replacement()

    # ffmpeg -i input.avi -c:v libx264 -preset slow -crf 22 -c:a copy output.mkv
    def archive(self, threads=None):

        if os.path.exists(self.video):

            video_basename = os.path.splitext(os.path.split(self.video)[1])[0]
            replacement_video = os.path.join(
                os.path.dirname(self.video),
                f"{video_basename}.mkv",
            )

            with tempfile.TemporaryDirectory(dir=os.path.dirname(self.video)) as temp_dir:
                temp_vid_dest = os.path.join(
                    temp_dir,
                    f"{video_basename}.mkv",
                )

                # base command is just the executable
                cmd_base = [self.ffmpeg]

                # add onto the command if we need to limit threads
                if threads:
                    cmd_base = cmd_base + ["-threads", str(threads)]

                # add the rest of the command
                cmd = cmd_base + [
                    "-v", "error",
                    "-i", self.video,
                    "-c:v", "libx264",
                    "-preset", "slow",
                    "-crf", "28",
                    "-c:a", "mp3",
                    temp_vid_dest,
                ]
                ffproc = subprocess.run(cmd, capture_output=True)

                if ffproc.returncode != 0:

                    self.archive_success = False

                    print(f"Error in ffmpeg conversion: {self.video}{os.linesep}", flush=True)
                    print(f"stdout:{os.linesep}{ffproc.stdout}{os.linesep}", flush=True)
                    print(f"stderr:{os.linesep}{ffproc.stderr}{os.linesep}", flush=True)
                    
                    # raise an exception if we need to crash on failure.
                    if self.crash_on_fail:
                        ffproc.check_returncode()

                else:
                    self.archive_success = True

                os.rename(temp_vid_dest, replacement_video)
                self.replacement = replacement_video

            video_stat = os.stat(self.video)
            os.chown(self.replacement, video_stat.st_uid, video_stat.st_gid)
            os.chmod(self.replacement, video_stat.st_mode)

        else:
            raise FileNotFoundError(f"file {self.video} not found")


    # -v quiet -print_format json -show_format -show_streams
    def probe(self, replacement=False):


        if replacement:
            to_probe = self.replacement
        else:
            to_probe = self.video

        if os.path.exists(to_probe):

            cmd = [
                self.ffprobe, "-v", "quiet", "-print_format", "json",
                "-show_format", "-show_streams", to_probe,
            ]


            p = subprocess.run(cmd, capture_output=True)
            if self.crash_on_fail:
                p.check_returncode()

            if replacement:
                self.replacement_info = json.loads(p.stdout)
            else:
                self.video_info = json.loads(p.stdout)

        else:
            raise FileNotFoundError(f"file {to_probe} not found")

    
    def process(self, threads=None):

        run_transcode = False

        if not self.video_info:
            self.probe()

        try:

            # loop through the streams
            for vstream in self.video_info['streams']:

                # run transcodes when the video codec matches
                to_convert = ['mjpeg']
                if (vstream['codec_type'] == 'video') and (vstream['codec_name'] in to_convert):
                    run_transcode = True

            if run_transcode:
                self.archive(threads=threads)

                if self.archive_success:
                    self.delete_largest()

        except KeyError:
            if self.crash_on_fail:
                raise MissingFFProbeInfo("Missing FFProbe Data")

    def __init__(self, video, ffprobe="ffprobe", ffmpeg="ffmpeg", crash_on_fail=True):

        if not os.path.exists(video):
            raise FileNotFoundError(f"video requested does not exist: {video}")

        self.video = video
        self.replacement = None
        self.ffprobe = ffprobe
        self.ffmpeg = ffmpeg
        self.video_info = None
        self.replacement_info = None
        self.crash_on_fail = crash_on_fail
        self.archive_success = None

class DirectoryScanner:
        

    def __init__(self, directory, file_extensions=None):

        if not file_extensions:
            self.file_exensions = [".avi"]
        else:
            self.file_extensions = file_extensions

        if not os.path.isdir(directory):
            raise FileNotFoundError("The directory passed does not exist.")
        else:
            self.directory = directory

        self.found_files = []
        for filename in Path(directory).glob('**/*.*'):
            if os.path.splitext(filename)[1].lower() in file_extensions:
                #this_file = FoundVideo(filename)
                self.found_files.append(filename)

if __name__ == '__main__':

    default_extensions = [".avi"]

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--directory", help="directory used for scanning")
    parser.add_argument("-e", "--extensions", default=default_extensions, nargs="*",
        help="extensions to convert")
    parser.add_argument("-n", "--new", default=False, action="store_true",
        help="delete the original and keep the transcode.")
    parser.add_argument("-s", "--smaller", default=False, action="store_true",
        help="keep the smaller file between the original and transcode.")
    parser.add_argument("-p", "--process", default=False, action="store_true",
        help="smart processing")
    parser.add_argument("-l", "--list", default=False, action="store_true",
        help="list only - take no action")
    parser.add_argument("-t", "--threads", default=None, type=int, help="thread limit.")
    parser.add_argument("--no-crash", dest="crash_on_fail", default=True, action="store_false",
        help="Do not crash after failed ffmpeg conversion.")
    args = parser.parse_args()

    scanner = DirectoryScanner(args.directory, args.extensions)

    if args.list:
        print("List Mode:", flush=True)
        for file in scanner.found_files:
            print(f" - {file}", flush=True)
    
    else:
        # transcode options:
        for file in scanner.found_files:

            if os.path.exists(file):
                    
                # print and init object
                #print(f" - {file}")
                loop_vid = FoundVideo(file, crash_on_fail=args.crash_on_fail)
                
                if args.process:
                    # run the smart processor
                    print(f"Processing file {loop_vid.video}.", flush=True)
                    loop_vid.process(threads=args.threads)

                else:

                    # dumb probing and transcoding
                    print(f"Archiving file {loop_vid.video}.", flush=True)
                    loop_vid.probe()
                    loop_vid.archive(threads=args.threads)

                    if args.smaller:
                        # delete the smaller of the two
                        loop_vid.delete_largest()
                    elif args.new:
                        # delete the original
                        loop_vid.delete_original()
                    # else: leave both

                # cleanup
                del loop_vid

            else:
                print(f"Missing file {file}.", flush=True)
