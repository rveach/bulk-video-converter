#! /usr/bin/env python

import os
import pprint
import shutil
import subprocess
import sys

from nose.tools import with_setup, nottest, raises
from subprocess import CalledProcessError

# print(os.path.realpath(__file__))
# print(os.path.join(
#     os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
#     "src",
# ))

REPO_SRC_DIR = os.path.join(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
    "src",
)
sys.path.insert(0, REPO_SRC_DIR)

#pylint: disable=import-error
from bulk_converter import FoundVideo, UnableToDeleteFile

@nottest
def get_test_vid_src_location(filename):
    return os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "video",
        filename,
    )

@nottest
def get_test_vid_temp_location(filename):
    return os.path.join(
        get_temp_test_dirname(),
        filename,
    )

@nottest
def get_temp_test_dirname():
    return os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "temp_test_video_directory",
    )    

@nottest
def setup_temp_dir():

    os.mkdir(get_temp_test_dirname())

    all_test_videos = [
        "test_AW_20190308_123650A.avi",
        "test_AW_20190308_123650B.avi",
    ]

    for filename in all_test_videos:
        shutil.copyfile(
            get_test_vid_src_location(filename),
            get_test_vid_temp_location(filename),
        )

@nottest
def teardown_temp_dir():
    shutil.rmtree(get_temp_test_dirname())

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_convert():
    """
        Test conversion
    """

    # find video and init
    video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    assert os.path.exists(video_file)
    video = FoundVideo(video_file)

    # probe for info
    video.probe()
    assert 'format' in video.video_info.keys()
    assert 'streams' in video.video_info.keys()

    # archive the video
    video.archive()
    assert video.replacement != None
    video_repacement = video.replacement
    assert os.path.exists(video_repacement)

    # remove original
    video.delete_original()
    assert not os.path.exists(video_file)

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_process_single_thread():
    """
        Test transcoding with a single thread.
    """

    video_file = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    assert os.path.exists(video_file)
    video = FoundVideo(video_file)

    # process video
    video.process(threads=1)

    # check the formats
    assert 'format' in video.video_info.keys()
    assert 'streams' in video.video_info.keys()

    # make sure new video is there
    assert video.replacement != None
    assert os.path.exists(video.replacement)

    # make sure original is gone
    assert not os.path.exists(video_file)


@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_cmd_list():
    """
        Run on the cli to list found files.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "--list",
        "-d", dirname,
    ]
    subprocess.run(cmd, check=True)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)
    assert not os.path.exists(fileA.replace(".avi", ".mkv"))
    assert not os.path.exists(fileB.replace(".avi", ".mkv"))


@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_cmd_process():
    """
        Run on the cli with the smart process.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "--process",
        "-d", dirname,
    ]
    subprocess.run(cmd, check=True)

    assert not os.path.exists(fileA)
    assert not os.path.exists(fileB)
    assert os.path.exists(fileA.replace(".avi", ".mkv"))
    assert os.path.exists(fileB.replace(".avi", ".mkv"))

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_cmd_new():
    """
        Run on the cli to ensure that we keep the transcode copy.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "--new",
        "-d", dirname,
    ]
    subprocess.run(cmd, check=True)

    assert not os.path.exists(fileA)
    assert not os.path.exists(fileB)
    assert os.path.exists(fileA.replace(".avi", ".mkv"))
    assert os.path.exists(fileB.replace(".avi", ".mkv"))

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_cmd_smaller():
    """
        Run test on cli to ensure the smallest of the two is kept.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "--smaller",
        "-d", dirname,
    ]
    subprocess.run(cmd, check=True)

    assert not os.path.exists(fileA)
    assert not os.path.exists(fileB)
    assert os.path.exists(fileA.replace(".avi", ".mkv"))
    assert os.path.exists(fileB.replace(".avi", ".mkv"))

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_cmd_keep_both():
    """
        Run script on cli to make sure both videos are kept.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "-d", dirname,
    ]
    subprocess.run(cmd, check=True)

    assert os.path.exists(fileA)
    assert os.path.exists(fileB)
    assert os.path.exists(fileA.replace(".avi", ".mkv"))
    assert os.path.exists(fileB.replace(".avi", ".mkv"))

@raises(FileNotFoundError)
def test_init_missing_file():
    """
        make sure we raise an exception when we init the class without
        a file present.
    """

    video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    assert not os.path.exists(video_file)
    FoundVideo(video_file)

@with_setup(setup_temp_dir, teardown_temp_dir)
@raises(FileNotFoundError)
def test_probe_missing_file():
    """
        Make sure we raise an exception when we try to probe a missing file.
    """

    try:
        video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
        assert os.path.exists(video_file)
        video = FoundVideo(video_file)
    except FileNotFoundError:
        pass # make sure we don't trigger here

    os.remove(video_file)
    assert not os.path.exists(video_file)
    video.probe()

@with_setup(setup_temp_dir, teardown_temp_dir)
@raises(FileNotFoundError)
def test_archive_missing_file():
    """
        Make sure we raise an exception if we try to archive a missing file.
    """

    try:
        video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
        assert os.path.exists(video_file)
        video = FoundVideo(video_file)
    except FileNotFoundError:
        pass # make sure we don't trigger here

    os.remove(video_file)
    assert not os.path.exists(video_file)
    video.archive()


@with_setup(setup_temp_dir, teardown_temp_dir)
@raises(CalledProcessError)
def test_ffmpeg_error_crash():
    """
        Raise an exception when ffmpeg fails to process
    """

    try:
        video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
        assert os.path.exists(video_file)
        video = FoundVideo(video_file)
    except FileNotFoundError:
        pass # make sure we don't trigger here

    fo = open(video_file, "r+")
    fo.truncate()
    fo.close()
    assert os.path.exists(video_file)
    assert os.stat(video_file).st_size == 0
    video.archive()

@with_setup(setup_temp_dir, teardown_temp_dir)
@raises(UnableToDeleteFile)
def test_prevent_delete_replacement():
    """
        Truncating the original file then attempting to delete the replacement.
    """

    try:
        video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
        assert os.path.exists(video_file)
        video = FoundVideo(video_file)
    except FileNotFoundError:
        pass # make sure we don't trigger here

    # archive the file to create the replacement.
    video.archive()

    # truncate the original
    fo = open(video_file, "r+")
    fo.truncate()
    fo.close()
    assert os.path.exists(video_file)
    assert os.stat(video_file).st_size == 0

    # try to delete the replacement - should trigger UnableToDeleteFile
    video.delete_replacement()
    assert os.path.exists(video.replacement)


@with_setup(setup_temp_dir, teardown_temp_dir)
@raises(UnableToDeleteFile)
def test_prevent_delete_original():
    """
        Truncating the original file then attempting to delete the replacement.
    """

    try:
        video_file = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
        assert os.path.exists(video_file)
        video = FoundVideo(video_file)
    except FileNotFoundError:
        pass # make sure we don't trigger here

    # archive the file to create the replacement.
    video.archive()

    # truncate the replacement
    fo = open(video.replacement, "r+")
    fo.truncate()
    fo.close()
    assert os.path.exists(video.replacement)
    assert os.stat(video.replacement).st_size == 0

    # try to delete the replacement - should trigger UnableToDeleteFile
    video.delete_original()
    assert os.path.exists(video.video)

@with_setup(setup_temp_dir, teardown_temp_dir)
def test_run_dont_crash():
    """
        Run script on cli to make sure both videos are kept.
    """

    fileA = get_test_vid_temp_location("test_AW_20190308_123650A.avi")
    fileB = get_test_vid_temp_location("test_AW_20190308_123650B.avi")
    dirname = os.path.dirname(fileA)

    # truncate one of the files
    fo = open(fileA, "r+")
    fo.truncate()
    fo.close()
    assert os.path.exists(fileA)
    assert os.stat(fileA).st_size == 0

    assert os.path.exists(fileB)
    assert os.stat(fileA).st_size >= 0

    cmd = [
        "python", os.path.join(REPO_SRC_DIR, "bulk_converter.py"),
        "-d", dirname, "--no-crash", "--process",
    ]
    subprocess.run(cmd, check=True)

    assert os.path.exists(fileA)
    assert not os.path.exists(fileB)
    assert not os.path.exists(fileA.replace(".avi", ".mkv"))
    assert os.path.exists(fileB.replace(".avi", ".mkv"))